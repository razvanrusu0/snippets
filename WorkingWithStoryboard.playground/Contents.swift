import UIKit

public protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

/// Assign the class name of the view controller as the storyboard identifier
extension UIViewController: StoryboardIdentifiable {
    public static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

public extension UIStoryboard {
    ///
    func instantiateViewController<T: UIViewController>() -> T  {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Could not instantiate view controller with identifier: \(T.storyboardIdentifier)")
        }
        
        return viewController
    }
}

/// Convenience enum for storing strongly typed storyboard names
enum Storyboard: String {
    case main = "Main"
}

///
extension UIStoryboard {
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
}

// Now we can quickly intantiate a view controller in a strongly typed manner
final class MyViewController: UIViewController {
    static func instantiate() -> MyViewController {
        let storyboard = UIStoryboard(storyboard: .main)
        let controller: MyViewController = storyboard.instantiateViewController()
        return controller
    }
}

