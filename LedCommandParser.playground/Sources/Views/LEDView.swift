import UIKit

@IBDesignable
public  class LEDView: UIView {
    @IBInspectable let leftLed: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    @IBInspectable let ambianceLed: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    @IBInspectable let rightLed: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    @IBInspectable var leftLedColor: UIColor = UIColor.green {
        didSet {
            leftLed.backgroundColor = leftLedColor
        }
    }
    
    @IBInspectable var rightLedColor: UIColor = UIColor.red {
        didSet {
            rightLed.backgroundColor = rightLedColor
        }
    }
    
    @IBInspectable var ambianceLedColor: UIColor = UIColor.lightGray {
        didSet {
            ambianceLed.backgroundColor = ambianceLedColor
        }
    }
    
    private var blinkTimer: Timer?
    
    public var blinkTimeInterval: TimeInterval = 0.2
    
    public var currentProgram: LEDProgram? {
        didSet {
            leftLedOn = true
            rightLedOn = true
            shouldStopBlinking = false
        }
    }
    
    private(set) var leftLedOn: Bool = true {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.leftLed.backgroundColor = self.leftLedOn ? self.leftLedColor : .gray
                
            }
        }
    }
    
    private(set) var rightLedOn: Bool = true {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.rightLed.backgroundColor = self.rightLedOn ? self.rightLedColor : .gray
            }
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        addSubview(leftLed)
        addSubview(ambianceLed)
        addSubview(rightLed)
        
        leftLed.backgroundColor     = leftLedColor
        rightLed.backgroundColor    = rightLedColor
        ambianceLed.backgroundColor = ambianceLedColor
        
        let layoutConstraints = [
            leftLed.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1, constant: -8),
            leftLed.widthAnchor.constraint(equalTo: leftLed.heightAnchor, multiplier: 1, constant: 0),
            leftLed.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            leftLed.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            leftLed.trailingAnchor.constraint(equalTo: ambianceLed.leadingAnchor, constant: -8),
            
            ambianceLed.widthAnchor.constraint(equalTo: ambianceLed.heightAnchor, multiplier: 1, constant: 0),
            ambianceLed.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0),
            ambianceLed.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            ambianceLed.trailingAnchor.constraint(equalTo: rightLed.leadingAnchor, constant: -8),
            
            rightLed.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1, constant: -8),
            rightLed.widthAnchor.constraint(equalTo: rightLed.heightAnchor, multiplier: 1, constant: 0),
            rightLed.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
            rightLed.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0)
        ]
        
        layoutConstraints.forEach { $0.isActive = true }
    }
    
    private var shouldStopBlinking: Bool = false {
        didSet {
            leftLedOn = true
            rightLedOn = true
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius              = bounds.height / 2.0
        layer.masksToBounds             = true
        
        leftLed.layer.cornerRadius      = leftLed.bounds.height / 2.0
        leftLed.layer.masksToBounds     = true
        
        rightLed.layer.cornerRadius     = rightLed.bounds.height / 2.0
        rightLed.layer.masksToBounds    = true
        
        ambianceLed.layer.cornerRadius  = ambianceLed.bounds.height / 2.0
        ambianceLed.layer.masksToBounds = true
    }
    
    public func startBlinking() {
        guard let program = currentProgram else {
            return
        }
        
        shouldStopBlinking = false
        let commands = program.commands
        let shouldLoop = program.shouldLoop
    
        _startBlinking(for: commands, shouldLoop: shouldLoop)
    }
    
    func stopBlinking() {
        shouldStopBlinking = false
    }
    
    private func _startBlinking(for commands: [LEDCommand], shouldLoop: Bool) {
        runCommands(commands: commands) {
            if shouldLoop && !self.shouldStopBlinking {
                self._startBlinking(for: commands, shouldLoop: shouldLoop)
            }
        }
    }
    
    private func runCommands(commands: [LEDCommand], didFinish: @escaping () -> Void) {
        if shouldStopBlinking {
            didFinish()
            return
        }
        
        guard commands.count > 0 else {
            didFinish()
            return
        }
        
        let command = commands[0]
        leftLedOn = command.combination.greenOn
        rightLedOn = command.combination.redOn
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(command.duration)) { [weak self] in
            guard let `self` = self else {
                didFinish()
                return
            }
            
            guard !self.shouldStopBlinking else {
                didFinish()
                return
            }
            
            let newCommands = Array(commands[1..<commands.count])
            self.runCommands(commands: newCommands, didFinish: didFinish)
        }
    }
    
    deinit {
        blinkTimer?.invalidate()
        blinkTimer = nil
    }
}
