import Foundation

///
public struct LEDProgram {
    public let rawMessage: String
    public let commands: [LEDCommand]
    public let shouldLoop: Bool
    
    public init(message: String) {
        self.rawMessage = message
        var splittedMessage = message.components(separatedBy: "-")
        
        if let lastCommand = splittedMessage.last, lastCommand.compare("L") == .orderedSame {
            self.shouldLoop = true
            let _ = splittedMessage.removeLast()
        } else {
            self.shouldLoop = false
        }
        
        self.commands = LEDProgram.commandsFrom(rawMessages: splittedMessage)
    }
    
    private static func commandsFrom(rawMessages: [String]) -> [LEDCommand] {
        return rawMessages
            .map {
                $0.components(separatedBy: ".")
            }
            .compactMap {
                guard
                    $0.count == 2,
                    let combination = LEDCombination(rawValue: $0[0]),
                    let duration = Int($0[1]) else {
                        return nil
                }
                return LEDCommand(combination: combination, duration: duration)
        }
    }
}
