import Foundation

/// Used for telling the led program what led
/// combination should be shown and how long
/// should it be active
public struct LEDCommand {
    public let combination: LEDCombination
    public let duration: Int
}
