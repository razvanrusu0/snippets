import Foundation

/// Convenience enum used for storing all Green and Red combinations
public enum LEDCombination: String {
    case redOn       = "R"
    case redOff      = "r"
    case greenOn     = "G"
    case greenOff    = "g"
    case redGreenOn  = "RG"
    case redGreenOff = "rg"
    
    public var redOn: Bool {
        switch self {
        case .redOff, .greenOn, .greenOff, .redGreenOff:
            return false
        case .redOn, .redGreenOn:
            return true
        }
    }
    
    public var greenOn: Bool {
        switch self {
        case .redOn, .redOff, .greenOff, .redGreenOff:
            return false
        case .greenOn, .redGreenOn:
            return true
        }
    }
}
