import UIKit
import PlaygroundSupport

/*
 We will consider a Led Combination any of the following strings:
 
 redOn       = "R"
 redOff      = "r"
 greenOn     = "G"
 greenOff    = "g"
 redGreenOn  = "RG"
 redGreenOff = "rg"
 
 We wil construct a LED Command using
 - a combination
 - a duration (milliseconds)
 
 The loop command will be the string "L"
 
 The commands will be separated separated by "-"
 A command combination is separated from the duration using "."
 
 Considering these eaxmples:
 
 1. R02r08L: Red LED blinks for 200ms with a period (repetition) of one second (0.2 + 0.8 = 1 second)
 2. R01r04G01g04L: Short alternate red and green flash for 0.2 seconds with the period of 1 second
 3. RG02rg08L: short flash of both leds for 0.2 seconds, each second.
 
 We can convert it using the new LED protocol
 
 1. R.200-r.800-L
 2. R.100-r.400-G.100-g-400-L
 3. RG.200-rg.800-L
 
 The commands will be executed in a serial manner (first command, second command, third command)
 The parser will not consider the following combinations: Rg, rG, Gr and gR
 */

// 5 red blinks
//let message = "R.100-r.300-R.100-r.300-R.100-r.300-R.100-r.300-R.100-r.3000-L"

// An alternating green / red light
let message = "G.200-g.200-R.200-r.200-L"

let ledView = LEDView(frame: CGRect(origin: .zero, size: CGSize(width: 140, height: 55)))
ledView.currentProgram = LEDProgram(message: message)
ledView.startBlinking()

PlaygroundPage.current.needsIndefiniteExecution = true
PlaygroundPage.current.liveView = ledView
